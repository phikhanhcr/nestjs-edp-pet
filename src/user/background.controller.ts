import { Controller } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { UserQueueBackgroundService } from './background.queue.service';

@Controller()
export class BackgroundController {
  constructor(
    private readonly userBackgroundService: UserQueueBackgroundService,
  ) {}

  @OnEvent('user.registered')
  handleUserRegistration(payload: any) {
    this.userBackgroundService.sendEmailGreeting(payload);
  }
}
