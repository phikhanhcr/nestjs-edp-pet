import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class UserService {
  constructor(private eventEmitter: EventEmitter2) {}
  register({ name }: { name: string }): string {
    //
    this.eventEmitter.emit('user.registered', { name });
    return 'Register user';
  }
}
